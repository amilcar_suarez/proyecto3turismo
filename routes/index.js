var ruta=require('express').Router();

module.exports=(function(app){
	var usuario=require('../ControladorUsuario.js')(app);
	var hotel=require('../ControladorHotel.js')(app);
	var habitacion=require('../ControladorHabitacion.js')(app);
	var departamento=require('../ControladorDepartamento.js')(app);
	var estancia=require('../ControladorEstancia.js')(app);
	var lugarturistico=require('../ControladorLugarTuristico.js')(app);
	var factura=require('../ControladorFactura.js')(app);
	var reservacion=require('../ControladorReservacion.js')(app);
	var tipohabitacion=require('../ControladorTipoHabitacion.js')(app);
	
	ruta.get('/', function(peticion, respuesta){
		respuesta.send("Servicio inicia");	
	});
	
	//ruta usuario
	ruta.post('/usuario/registro',usuario.registro);
	ruta.post('/usuario/login',usuario.login);
	
	//ruta factura
	ruta.get('/factura',factura.list);
	ruta.post('/factura',factura.add);
	ruta.put('/factura',factura.edit);
	ruta.delete('factura',factura.delete);
	
	//ruta hotel
	ruta.get('/hotel',hotel.list);
	ruta.post('/hotel',hotel.add);
	ruta.put('/hotel',hotel.edit);
	ruta.delete('hotel',hotel.delete);
	
	//ruta habitacion
	ruta.get('/habitacion',habitacion.list);
	ruta.post('/habitacion',habitacion.add);
	ruta.put('/habitacion',habitacion.edit);
	ruta.delete('/habitacion',habitacion.delete);
	
	//ruta departamento
	ruta.get('/departamento',departamento.list);
	ruta.post('departamento',departamento.add);
	ruta.put('/departamento',departamento.edit);
	ruta.delete('/departamento',departamento.delete);
	
	//ruta estancia
	ruta.get('/estancia',estancia.list);
	ruta.post('/estancia',estancia.add);
	ruta.put('/estancia',estancia.edit);
	ruta.delete('/estancia',estancia.delete);
	
	//ruta lugar turistico
	ruta.get('/lugarturistico',lugarturistico.list);
	ruta.post('/lugarturistico',lugarturistico.add);
	ruta.put('lugarturistico',lugarturistico.edit);
	ruta.delete('lugarturistico',lugarturistico.delete);
	
	//ruta reservacion
	ruta.get('/reservacion',reservacion.list);
	ruta.post('/reservacion',reservacion.add);
	ruta.put('/reservacion',reservacion.edit);
	ruta.delete('/reservacion',reservacion.delete);
	
	//ruta tipo habitacion
	ruta.get('/tipohabitacion',tipohabitacion.list);
	ruta.post('tipohabitacion',tipohabitacion.add);
	ruta.put('tipohabitacion',tipohabitacion.edit);
	ruta.delete('tipohabitacion',tipohabitacion.delete);
	
	return ruta;
	
});