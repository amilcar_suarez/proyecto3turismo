module.exports=function(app){
	return{
		add:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
					connection.release();
					res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
				}
				connection.query("INSERT INTO estancia VALUES (NULL,'"+req.body.fechaEntrada+"','"+req.body.fechaSalida+"','"+req.body.idUsuario+"');", function(err,row){
				if(err)
					throw err;
				else
					res.json({"mensaje":"Estancia Agregado"});
				connection.release();
				});
			});
		},
				delete:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Delete from estancia where idEstancia="+req.body.idEstancia, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Estancia eliminada"});
					connection.release();	
				});
			});	
		},
		list:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Select * from estancia where idEstancia="+req.query.idEstancia, function(err, row){
					if(err)
						throw err;
					else
						res.json(row);
					connection.release();	
				});
			});	
		},
		edit:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("UPDATE estancia set fechaEntrada='"+req.body.fechaEntrada+"',fechaSalida="+req.body.fechaSalida+"' where estancia="+req.body.idEstancia, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Estancia editada"});
					connection.release();	
				});
			});	
		}
	}
}