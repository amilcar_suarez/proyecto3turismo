module.exports=function(app){
	return{
		add:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
					connection.release();
					res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
				}
				connection.query("INSERT INTO lugar VALUES (NULL,'"+req.body.nombre+"','"+req.body.numero+"','"+req.body.personas+"','"+req.body.costo+"','"+req.body.descripcion+"','"+req.body.idHotel+"','"+req.body.idReservacion+"','"+req.body.idEstancia+"','"+req.body.idTipoHabitacion+"');", function(err,row){
				if(err)
					throw err;
				else
					res.json({"mensaje":"Habitacion Agregada"});
				connection.release();
				});
			});
		},
				delete:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Delete from habitacion where idHabitacion="+req.body.idHabitacion, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Habitacion eliminada"});
					connection.release();	
				});
			});	
		},
		list:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Select * from habitacion where idHabitacion="+req.query.idHabitacion, function(err, row){
					if(err)
						throw err;
					else
						res.json(row);
					connection.release();	
				});
			});	
		},
		edit:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("UPDATE habitacion set nombre='"+req.body.nombre+"',descripcion="+req.body.descripcion+",idDepartamento="+req.body.idDepartamento+"' where idLugarTuristico="+req.body.idLugarTuristico, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Habitacion editada"});
					connection.release();	
				});
			});	
		}
	}
}