CREATE DATABASE DB_TURISMO;

USE DB_TURISMO;


CREATE TABLE Usuario(
	idUsuario int(10) NOT NULL AUTO_INCREMENT,
  nombre varchar(128) NOT NULL,
  correo varchar(64) NOT NULL,
  nick varchar(128) NOT NULL,
  clave varchar(128) NOT NULL,
  PRIMARY KEY (idUsuario)
);

CREATE TABLE Rol(
	idRol int(10) NOT NULL AUTO_INCREMENT,
  nombre varchar(128) DEFAULT NULL,
  PRIMARY KEY (idRol),
  idUsuario int NOT NULL,
  FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario)
);


CREATE TABLE Departamento(
	idDepartamento int(10) NOT NULL AUTO_INCREMENT,
  nombre varchar(128) NOT NULL,
  PRIMARY KEY (idDepartamento)

);

CREATE TABLE LugarTuristico(
	idLugarTuristico int(10) NOT NULL AUTO_INCREMENT,
  nombre varchar(128) NOT NULL,
  descripcion varchar(255) NOT NULL,
  idDepartamento int NOT NULL,
  PRIMARY KEY (idLugarTuristico),
  FOREIGN KEY (idDepartamento) REFERENCES Departamento(idDepartamento)

);


CREATE TABLE Hotel(
	idHotel int(10) NOT NULL AUTO_INCREMENT,
  nombre varchar(128) NOT NULL,
  idDepartamento int NOT NULL,
  PRIMARY KEY (idHotel),
  FOREIGN KEY (idDepartamento) REFERENCES Departamento(idDepartamento)
  
);

CREATE TABLE Estancia(
	idEstancia int(10) NOT NULL AUTO_INCREMENT,
  fechaEntrada date NOT NULL,
  fechaSalida date NOT NULL,
  idUsuario int NOT NULL,
  PRIMARY KEY (idEstancia),
  FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario)

);

CREATE TABLE TipoHabitacion(
	idTipoHabitacion int(10) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) NOT NULL,
  descripcion varchar(255) NOT NULL,
  PRIMARY KEY (idTipoHabitacion)

);

CREATE TABLE Reservacion(
	idReservacion int(10) NOT NULL AUTO_INCREMENT,
  idUsuario int NOT NULL,
  idEstancia int NOT NULL,
  PRIMARY KEY (idReservacion),
  FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario),
  FOREIGN KEY (idEstancia) REFERENCES Estancia(idEstancia)
  
);

CREATE TABLE Habitacion(
	idHabitacion int(10) NOT NULL AUTO_INCREMENT,
  nombre varchar(128) DEFAULT NULL,
  numero int NOT NULL,
  personas int NOT NULL,
  costo DECIMAL NOT NULL,
  descripcion varchar(255) DEFAULT NULL,
  idHotel int NOT NULL,
  idReservacion int NOT NULL,
  idEstancia int NOT NULL,
  idTipoHabitacion int NOT NULL,
  PRIMARY KEY (idHabitacion),
  FOREIGN KEY (idHotel) REFERENCES Hotel(idHotel),
  FOREIGN KEY (idTipoHabitacion) REFERENCES TipoHabitacion(idTipoHabitacion),
  FOREIGN KEY (idEstancia) REFERENCES Estancia(idEstancia),
  FOREIGN KEY (idReservacion) REFERENCES Reservacion(idReservacion)
 
 
  
);

CREATE TABLE Factura(
	idFactura int(10) NOT NULL AUTO_INCREMENT,
  idUsuario int NOT NULL,
  fechaHora datetime NOT NULL,
  total DECIMAL NOT NULL,
  PRIMARY KEY (idFactura),
  FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario)
  
);