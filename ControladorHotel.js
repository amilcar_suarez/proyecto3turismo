module.exports=function(app){
	return{
		add:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
					connection.release();
					res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
				}
				connection.query("INSERT INTO hotel VALUES (NULL,'"+req.body.nombre +"','"+req.body.idDepartamento+"');", function(err,row){
				if(err)
					throw err;
				else
					res.json({"mensaje":"Hotel Agregado"});
				connection.release();
				});
			});
		},
				delete:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Delete from hotel where idHotel="+req.body.idHotel, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Hotel eliminado"});
					connection.release();	
				});
			});	
		},
		list:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Select * from hotel where idHotel="+req.query.idHotel, function(err, row){
					if(err)
						throw err;
					else
						res.json(row);
					connection.release();	
				});
			});	
		},
		edit:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("UPDATE hotel set nombre='"+req.body.nombre+"',idDepartamento="+req.body.idDepartamento+"' where idHotel="+req.body.idHotel, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Hotel editado"});
					connection.release();	
				});
			});	
		}
	}
}