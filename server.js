(function (){
	var express=require('express');
	var bodyParser=require('body-parser');
	var morgan=require('morgan');
	var mysql=require('mysql');
	var puerto=3000;
 	var conf=require('./config'); 
	var app=express();
	var Sequelize = require('sequelize');
	var sequelize = new Sequelize('db_turismo', 'root', '', {
		host: 'localhost',
		dialect: 'mysql',
		
		pool:{
		max: 20,
		min: 0,
		idle: 10000
		},
		
	});
	
	//var sequelize = new Sequelize('mysql://root::5432/db_turismo');
	
	var Rol = sequelize.define('rol', {
	idrol: {
	type: Sequelize.INTEGER,
	field: 'idRol'
	},
	nombre: {
	type: Sequelize.STRING,
	field: 'nombre'
	}
	}, {
		freezeTableName: true //Model tableName will be the same  as the model name
		});
		
		
		var Usuario = sequelize.define('usuario', {
		idUsuario: {
		type: Sequelize.INTEGER,
		field: 'idUsuario'
		},
		nombre: {
		type: Sequelize.STRING,
		field: 'nombre'
		},
		correo: {
		type: Sequelize.STRING,
		field: 'correo'
		},
		nick: {
		type: Sequelize.STRING,
		field: 'nick'
		},
		clave: {
		type: Sequelize.STRING,
		field: 'clave'
		},
		idRol:{
			type: Sequelize.INTEGER,
			field: 'idRol'
		}	
		}, {
		freezeTableName: true //Model tableName will be the same  as the model name
		});
		
		var LugarTuristico = sequelize.define('lugarturistico',{
		idLugarTuristico: {
		type: Sequelize.INTEGER,
		field: 'idLugarTuristico'
		},
		nombre: {
		type: Sequelize.STRING,
		field: 'nombre'
		},
		descripcion: {
		type: Sequelize.STRING,
		field: 'descripcion'
		}	
		}, {
		freezeTableName: true //Model tableName will be the same  as the model name
		});
		
		var Departamento = sequelize.define('departamento',{
		idDepartamento: {
		type: Sequelize.INTEGER,
		field: 'idDepartamento'
		},
		nombre: {
		type: Sequelize.INTEGER,
		field: 'nombre'
		}	
		}, {
		freezeTableName: true //Model tableName will be the same  as the model name
		});
		
		var Hotel = sequelize.define('hotel',{
		idHotel: {
		type: Sequelize.INTEGER,
		field: 'idHotel'
		},
		nombre: {
		type: Sequelize.STRING,
		field: 'nombre'
		},	
		idDepartamento: {
		type: Sequelize.INTEGER,
		field: 'idDepartamento'
		}
		}, {
		freezeTableName: true //Model tableName will be the same  as the model name
		});
		
			var Estancia = sequelize.define('estancia',{
		idEstanca: {
		type: Sequelize.INTEGER,
		field: 'idEstancia'
		},
		fechaEntrada: {
		type: Sequelize.DATEONLY,
		field: 'fechaEntrada'
		},
		fechaSalida: {
		type: Sequelize.DATEONLY,
		field: 'fechaSalida'
		},
		idUsuario:{
			type: Sequelize.INTEGER,
			field: 'idUsuario'
		}	
		}, {
		freezeTableName: true //Model tableName will be the same  as the model name
		});
		
			var Reservacion = sequelize.define('reservacion',{
		idReservacion: {
		type: Sequelize.INTEGER,
		field: 'idReservacion'
		},
		idUsuario: {
		type: Sequelize.INTEGER,
		field: 'idUsuario' 
		},
		idEstanca: {
		type: Sequelize.INTEGER,
		field: 'idEstanca'
		}	
		}, {
		freezeTableName: true //Model tableName will be the same  as the model name
		});
		
		var TipoHabitacion = sequelize.define('tipohabitacion',{
		idTipoHabitacion: {
		type: Sequelize.INTEGER,
		field: 'idTipoHabitacion'
		},
		nombre: {
		type: Sequelize.STRING,
		field: 'nombre'
		},
		descripcion: {
		type: Sequelize.STRING,
		field: 'descripcion'
		}	
		}, {
		freezeTableName: true //Model tableName will be the same  as the model name
		});
		
		var Habitacion = sequelize.define('habitacion',{
		idHabitacion: {
		type: Sequelize.INTEGER,
		field: 'idHabitacion'
		},
		nombre: {
		type: Sequelize.STRING,
		field: 'nombre'
		},
		numero: {
		type: Sequelize.INTEGER,
		field: 'numero'
		},
		personas: {
		type: Sequelize.INTEGER,
		field: 'personas'
		},
		costo: {
		type: Sequelize.DECIMAL,
		field: 'costo'
		},
		descripcion: {
		type: Sequelize.STRING,
		field: 'descripcion'
		},
		idHotel: {
		type: Sequelize.INTEGER,
		field: 'idHotel'
		},
		idEstancia: {
		type: Sequelize.INTEGER,
		field: 'idEstancia'
		},
		idHabitacion: {
		type: Sequelize.INTEGER,
		field: 'idHabitacion'
		},
		idTipoHabitacionHabitacion: {
		type: Sequelize.INTEGER,
		field: 'idTipoHabitacion'
		},
		idReservacion: {
		type: Sequelize.INTEGER,
		field: 'idReservacion'
		}
		}, {
		freezeTableName: true //Model tableName will be the same  as the model name
		});
		
		var Factura = sequelize.define('factura',{
		idFactura: {
		type: Sequelize.INTEGER,
		field: 'idFactura'
		},
		fechaHora: {
		type: Sequelize.DATE,
		field: 'fechaHora'
		},
		total: {
		type: Sequelize.DECIMAL,
		field: 'total'
		}
		}, {
		freezeTableName: true //Model tableName will be the same  as the model name
		});

		
		app.use(bodyParser.json());
		app.use(bodyParser.urlencoded({
			extended:true
		}));
		app.use(morgan('dev'));
		app.use('/api/v1/',require('./routes')(app));
		app.listen(puerto,function(){
			console.log("servidor iniciado en el puerto:"+puerto);
		});
	})();
		
		
		
		
		
		